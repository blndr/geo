# Geo
The purpose of this software is to learn the countries and capitals of the world !
Only in french !

## Usage

```
. venv/bin/activate
./run.py
```

## Install requirements
This project uses Python 3.6 and its typing mechanism.

```
. venv/bin/activate
pip install -r requirements.txt
```

## Unit test execution
```
. venv/bin/activate
pytest
```