import unittest

from app.geo import load_states, state_name_question, state_capital_question, correct_answer, wrong_answer, \
    is_answer_correct
from app.state import State


class GeoTest(unittest.TestCase):
    def test_load_states_reads_a_csv_file_and_returns_a_list_of_states(self):
        # when
        states = load_states()

        # then
        assert State('Colombie', 'Bogota') in states
        assert State('Grèce', 'Athènes') in states
        assert State('Nigeria', 'Abuja') in states

    def test_state_name_question_returns_a_question_based_on_the_capital(self):
        # given
        state = State('France', 'Paris')

        # when
        question = state_name_question(state)

        # then
        assert question == '> De quel pays \033[1mParis\033[0m est-elle la capitale ?'

    def test_state_capital_question_returns_a_question_based_on_the_state_name(self):
        # given
        state = State('France', 'Paris')

        # when
        question = state_capital_question(state)

        # then
        assert question == '> Quelle est la capitale de \033[1mFrance\033[0m ?'

    def test_correct_answer_returns_a_cheerful_message(self):
        # when
        message = correct_answer()

        # then
        assert message == 'Bonne réponse ! \033[1m\033[92m1 point\033[0m\033[0m gagné :-)'

    def test_wrong_answer_returns_a_message_with_the_expected_answer(self):
        # given
        expected_answer = 'Paris'

        # when
        message = wrong_answer(expected_answer)

        # then
        assert message == 'Mauvaise réponse ! C''était \033[1m\033[91mParis\033[0m\033[0m :-('

    def test_is_answer_correct_returns_true_if_given_value_is_correct_ignoring_case(self):
        # given
        answer = 'paRiS'

        # when
        result = is_answer_correct(answer, 'Paris')

        # then
        assert result is True

    def test_is_answer_correct_returns_true_if_given_value_is_correct_ignoring_dash_in_expected_answer(self):
        # given
        answer = 'nouvelle guinée'

        # when
        result = is_answer_correct(answer, 'nouvelle-guinée')

        # then
        assert result is True

    def test_is_answer_correct_returns_true_if_given_value_is_correct_ignoring_dash_in_given_answer(self):
        # given
        answer = 'nouvelle-guinée'

        # when
        result = is_answer_correct(answer, 'nouvelle guinée')

        # then
        assert result is True
