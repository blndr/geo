import unittest

from app.state import State


class StateTest(unittest.TestCase):
    def test_repr_should_be_human_readable(self):
        # given
        state = State('Suède', 'Stockholm')

        # when
        repr = state.__repr__()

        # then
        assert repr == 'Suède    ->    Stockholm'
