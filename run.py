#!/usr/bin/env python3
import os
import random

from app.format import bold, cyan
from app.geo import load_states, state_name_question, state_capital_question, ask_for_state_name, handle_turn

states = load_states()
random.shuffle(states)

score = 0
questions = input('Pour combien de questions veux-tu jouer ?\n')
choices = states[:int(questions)]
print('Ok, mémorise ces pays...')
for choice in choices:
    print(choice)

input('C\'est bon ?')
os.system('clear')
print('Ok, go !\n')

for choice in choices:
    state_question = ask_for_state_name()
    question = state_name_question if state_question else state_capital_question
    answer = choice.name if state_question else choice.capital
    score = handle_turn(choice, question, answer, score)
    print('\n')

print(bold('Score final : %s' % cyan('%d point(s)' % score)))
