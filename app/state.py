class State:
    def __init__(self, name, capital):
        self.name = name
        self.capital = capital

    def __eq__(self, other):
        return self.name == other.name and self.capital == other.capital

    def __repr__(self):
        return '%s    ->    %s' % (self.name, self.capital)
