import csv
import random
from typing import List

from app.format import bold, green, red
from app.state import State


def load_states() -> List[State]:
    with open('resources/data_set.csv') as file:
        raw_states = csv.reader(file, delimiter=',')
        return [State(row[0], row[1]) for row in raw_states]


def ask_for_state_name() -> bool:
    return random.randint(0, 100) % 2 == 0


def handle_turn(state: State, question_func, expected_answer: str, score: int) -> int:
    answer = input(question_func(state) + '\n')
    if is_answer_correct(answer, expected_answer):
        print(correct_answer())
        score += 1
    else:
        print(wrong_answer(expected_answer))
    return score


def state_name_question(state: State) -> str:
    return '> De quel pays %s est-elle la capitale ?' % bold(state.capital)


def state_capital_question(state: State) -> str:
    return '> Quelle est la capitale de %s ?' % bold(state.name)


def correct_answer() -> str:
    return 'Bonne réponse ! %s gagné :-)' % bold(green('1 point'))


def wrong_answer(expected_answer: str) -> str:
    return 'Mauvaise réponse ! C''était %s :-(' % bold(red(expected_answer))


def is_answer_correct(answer: State, expected: str) -> bool:
    return _sanitize_word(answer) == _sanitize_word(expected)


def _sanitize_word(word: str) -> str:
    return word.lower().replace('-', ' ')
