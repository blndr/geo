def green(message: str) -> str:
    return '\033[92m%s\033[0m' % message


def red(message: str) -> str:
    return '\033[91m%s\033[0m' % message


def cyan(message: str) -> str:
    return '\033[96m%s\033[0m' % message


def bold(message: str) -> str:
    return '\033[1m%s\033[0m' % message
